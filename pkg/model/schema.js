const mongoose = require('./config')();


const Project = new mongoose.model('Project',{
    name: String,
    isPublic: Boolean,
    owners: [String],
    files: [{
        name: String,
        createdAt: Date,
        updatedAt: Date,
        hash: String,
    }],
});

const User = new mongoose.model('User',{
    email: String,
    passwordSHA: String,
});

module.exports = {
    Project,
    User
}
