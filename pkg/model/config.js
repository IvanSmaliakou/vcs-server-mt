const mongoose = require('mongoose');

module.exports = () => {
  mongoose.connect('mongodb://localhost:27017/vcs_server', {
    useNewUrlParser: true
  })
    .catch((e) => {
      return console.error(`cannot connect to mongo: ${e.message}`);
    });
  return mongoose;
};
