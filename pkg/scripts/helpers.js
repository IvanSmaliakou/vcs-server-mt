const schema = require('../model/schema');

function isOwner(projectName, userEmail) {
  const project = schema.Project.findOne({ name: projectName });
  return project.owners.contains(userEmail);
}

module.exports = {
  isOwner
};
