const jwt = require('jsonwebtoken');
const createError = require('http-errors');
const sha = require('crypto-js/sha256');
const schema = require('../model/schema');

const {
  body,
  validationResult
} = require('express-validator');

module.exports = (authHandler) => {

  authHandler.post('/register', [
    body('email')
      .isEmail(),
    body('password')
      .notEmpty()
  ], async (req, res, next) => {
    const err = validationResult(req);
    if (!err.isEmpty()) {
      return next(createError(400, err.array()));
    }

    const body = req.body;
    const passwordHash = sha(body.password)
      .toString();
    let user = {
      email: body.email,
      passwordSHA: passwordHash,
    };
    try {
      var userEntity = await schema.User.findOne({ email: user.email }, '_id')
        .exec();
    } catch (e) {
      return next(createError(500, `cannot find user entity: ${e}`));
    }
    if (userEntity) {
      return next(createError(400, 'user already exists'));
    }
    try {
      var savedUser = await schema.User(user)
        .save();
    } catch (e) {
      return next(createError(500, `failed to save user: ${e}`));
    }
    console.log(`user with id ${savedUser._id} created`);
    res.status(200)
      .json({
        user: {
          id: savedUser._id,
          email: user.email
        }
      });
  });

  authHandler.post('/login', [
    body('email')
      .isEmail(),
    body('password')
      .notEmpty()
  ], async (req, res, next) => {
    const err = validationResult(req);
    if (!err.isEmpty()) {
      return next(createError(400, err.array()));
    }
    const body = req.body;
    const passwordHash = sha(body.password)
      .toString();

    let user = {
      email: body.email,
      passwordSHA: passwordHash,
    };
    try {
      var userEntity = await schema.User.findOne({ email: user.email });
    } catch (e) {
      return next(createError(500, `cannot find user entity: ${e}`));
    }
    if (!userEntity) {
      return next(createError(400, `user with email ${user.email} does not exist`));
    }
    if (userEntity.passwordSHA !== user.passwordSHA) {
      return next(createError(403, 'invalid password provided'));
    }
    res.json({
      token: jwt.sign({ email: user.email }, process.env.TOKEN_SECRET, { expiresIn: process.env.TOK_EXPIRES_IN }),
    });
    console.log(`user ${user.email} logged in successfully`);
  });
};
