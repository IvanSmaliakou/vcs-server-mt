const h = require('object-hash');
const createError = require('http-errors');
require('express-group-routes');

const schema = require('../model/schema');
const middleware = require('./middleware');
const ownerService = require('../service/owner');
const foreignerService = require('../service/foreigner');
const validator = require('../service/validator');

function allUsers(serviceHandler) {
    serviceHandler.get('/:projectName', async (req, res, next) => {
        let projectName = req.params.projectName;
        try {
            var project = await foreignerService.getProject(projectName);
        } catch (e) {
            return next(createError(500, e.message));
        }
        if (!project || !project.isPublic) {
            return next(createError(404, 'project is private or does not exist'));
        }
        return res.status(200)
            .json({
                project: {
                    id: project.id,
                    owners: project.owners,
                    isPublic: project.isPublic,
                    files: project.files
                },
            });
    });

    serviceHandler.group((authorizedHandler) => {
        authorizedHandler.use(middleware.authorize);

        authorizedHandler.post('/init', async (req, res, next) => {
                let createdProject = {};
                const project = {
                    name: req.body.name,
                    owners: req.body.owners,
                    isPublic: req.body.isPublic,
                };
                try {
                    await validator.validateProject(project)
                } catch (e) {
                    return next(createError(400, `validation error: ${e}`))
                }
                let projectEntity = await schema.Project(project);
                if (req.body.files) {
                    projectEntity.files = await req.body.files.map(file => {
                        let c = new Date(file.createdAt);
                        let u = new Date(file.updatedAt);

                        const fileObj = {
                            name: file.name,
                            createdAt: c,
                            updatedAt: u,
                        };
                        fileObj.hash = h(fileObj);
                        return fileObj;
                    });
                }
                try {
                    createdProject = await ownerService.init(projectEntity);
                } catch (e) {
                    return next(createError(500, `failed to init new project: ${e.message}`));
                }

                res.status(200)
                    .json({
                        project: {
                            id: createdProject._id,
                            name: createdProject.name,
                            files: createdProject.files,
                            isPublic: createdProject.isPublic,
                            owners: createdProject.owners,
                        },
                    });
            }
        );

        authorizedHandler.group((ownersOnlyHandler) => {
            ownersOnlyHandler.use(middleware.ownersOnly);

            ownersOnlyHandler.post('/:projectName/apply', async (req, res, next) => {
                let files = req.body.files;
                try {
                    await validator.validateFiles(files)
                } catch (e) {
                    return next(createError(400, `validation error: ${e}`))
                }
                try {
                    await ownerService.apply(files, req.params.projectName);
                } catch (e) {
                    return next(createError(500, `failed to apply files: ${e}`));
                }
                res.status(200)
                    .json({
                        files: files
                    });
            });

            ownersOnlyHandler.delete('/:projectName', async (req, res, next) => {
                try {
                    await ownerService.removeProject(req.params.projectName);
                } catch (e) {
                    return next(createError(500, `cannot remove project: ${e}`));
                }
                return res.status(200)
                    .json({
                        project: req.params.projectName
                    });
            });

            ownersOnlyHandler.delete('/:projectName/files', async (req, res, next) => {
                let files = req.body.files;
                try {
                     await ownerService.removeFiles(files, req.params.projectName);
                } catch (e) {
                    return next(createError(500, `cannot remove files from project ${req.params.projectName}: ${e}`));
                }
                res.status(200)
                    .json({
                        files: files.map(f => f.name),
                    });
            });
        });
    });
}

module.exports = {
    allUsers
};
