const jwt = require('jsonwebtoken');
const createError = require('http-errors');
const schema = require('../model/schema');

function authorize(req, res, next) {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  if (token == null) return next(createError(401));

  jwt.verify(token, process.env.TOKEN_SECRET, (err, payload) => {
    if (err) {
      return next(createError(403, `token verification has been failed: ${err}`));
    }
    req.body.email = payload.email;
    next();
  });
}

function ownersOnly(req, res, next) {
  const urlComponents = req.url.split('/');
  let projectName = '';
  if (urlComponents.length > 1) {
    projectName = urlComponents[1];
  }
  if (projectName === '') {
    return next(createError(400, 'empty project name provided'));
  }
  schema.Project.findOne({ name: projectName }, (err, res) => {
    if (err) {
      return next(createError(500, `couldn't get project by name ${projectName}: ${err}`));
    }
    if (!res) {
      return next(createError(400, `wrong project name: ${projectName}`));
    }
    if (res.owners.toObject()
      .includes(req.body.email)) {
      return next();
    }
    return next(createError(403, 'not enough permissions to access this resource'));
  });
}

function handleErrors(err, req, res, next) {
  res.status(err.statusCode)
    .json({
      error: err
    });
}

module.exports = {
  authorize,
  ownersOnly,
  handleErrors
};
