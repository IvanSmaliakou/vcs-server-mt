const schema = require('../model/schema');
const foreignerService = require('../service/foreigner');
const faker = require('faker');
const ownerService = require('../service/owner');
const middleware = require('../api/middleware');
const validator = require('../service/validator');
const mongoose = require('mongoose');
const config = require('../model/config');

afterEach(() => {
  jest.restoreAllMocks();
});

afterAll(() => {
  mongoose.connection.close();
});

describe('apply files', () => {
  it('should fail to findOneAndUpdate', async (done) => {
    expect.assertions(2);

    // setup
    let projName = faker.lorem.word();
    await schema.Project({
      name: projName,
      owners: [faker.internet.email()],
    })
      .save();

    await jest.spyOn(schema.Project, 'findOneAndUpdate')
      .mockImplementationOnce((query, update, params, cb) => {
        cb(Error());
      });

    try {
      var res = await ownerService.apply([{
        name: faker.lorem.word(),
        createdAt: faker.date.past(),
        updatedAt: faker.date.past(),
      }], projName);
    } catch (e) {
      expect(e)
        .toEqual('error while adding files to project: ');
    }
    expect(res)
      .toEqual(undefined);
    done();
  });

  it('should succeed with files', async (done) => {
    expect.assertions(1);

    // setup
    let projName = faker.lorem.word();
    let projectEntity = await schema.Project({
      name: projName,
      owners: [faker.internet.email()],
    })
      .save();

    await jest.spyOn(schema.Project, 'findOneAndUpdate')
      .mockImplementationOnce((query, update, params, cb) => {
        cb();
      });

    try {
      var res = await ownerService.apply([{
        name: faker.lorem.word(),
        createdAt: faker.date.past(),
        updatedAt: faker.date.past(),
      }], projName);
    } catch (e) {
      done.fail(`should succeed, but returns error: ${e.message}`);
    }
    expect(res)
      .toEqual(undefined);
    done();
  });

  it('should succeed without files', async (done) => {
    expect.assertions(1);

    // setup
    let projName = faker.lorem.word();
    let projectEntity = await schema.Project({
      name: projName,
      owners: [faker.internet.email()],
    })
      .save();

    await jest.spyOn(schema.Project, 'findOneAndUpdate')
      .mockImplementationOnce((query, update, params, cb) => {
        cb();
      });

    try {
      var res = await ownerService.apply(null, projName);
    } catch (e) {
      done.fail(`should succeed, but returns error: ${e.message}`);
    }
    expect(res)
      .toEqual(undefined);
    done();
  });
});

describe('init project', () => {
  it('should fail to find project', async (done) => {
    expect.assertions(1);

    // setup
    let projName = faker.lorem.word();
    let projectEntity = {
      name: projName,
      owners: [faker.internet.email()],
    };

    await jest.spyOn(schema.Project, 'findOne')
      .mockImplementationOnce((query, cb) => {
        cb(' ', null);
      });

    try {
      await ownerService.init(projectEntity);
    } catch (e) {
      expect(e.message)
        .toEqual(`couldn't get project by name ${projName}:  `);
    }
    done();
  });

  it('should fail to save project', async (done) => {
    expect.assertions(2);

    // setup
    let projName = faker.lorem.word();
    let projectEntity = {
      name: projName,
      owners: [faker.internet.email()],
    };

    await jest.spyOn(schema.Project, 'findOne')
      .mockImplementationOnce((query, cb) => {
        cb(null, projectEntity);
      });

    try {
      var res = await ownerService.init(projectEntity);
    } catch (e) {
      expect(e.message)
        .toEqual(`project ${projName} already exists`);
    }

    expect(res)
      .toEqual(undefined);
    done();
  });

  it('should fail because there is an error while saving a project', async (done) => {
    expect.assertions(2);

    // setup
    let projName = faker.lorem.word();
    let projectEntity = schema.Project({
      name: projName,
      owners: [faker.internet.email()],
    });

    await jest.spyOn(schema.Project, 'findOne')
      .mockImplementationOnce((query, cb) => {
        cb(null, null);
      });

    await jest.spyOn(projectEntity, 'save')
      .mockImplementationOnce((cb) => {
        cb(' ', null);
      });

    try {
      var res = await ownerService.init(projectEntity);
    } catch (e) {
      expect(e)
        .toEqual(`error while saving project ${projName}: undefined`);
    }
    expect(res)
      .toEqual(undefined);
    done();
  });

  it('should succeed to save', async (done) => {
    expect.assertions(1);

    // setup
    let projName = faker.lorem.word();
    let projectEntity = schema.Project({
      name: projName,
      owners: [faker.internet.email()],
    });

    await jest.spyOn(schema.Project, 'findOne')
      .mockImplementationOnce((query, cb) => {
        cb(null, null);
      });

    await jest.spyOn(projectEntity, 'save')
      .mockImplementationOnce((cb) => {
        cb(null, { projectEntity });
      });

    try {
      var res = await ownerService.init(projectEntity);
    } catch (e) {
      done.fail(`should succeed, but returns error: ${e.message}`);
    }
    expect(res.projectEntity)
      .toEqual(projectEntity);
    done();
  });
});

describe('remove files', () => {
  it('should fail to find files to remove', async (done) => {
    expect.assertions(1);

    // setup
    let files = new Array(50).fill('')
      .map(f => f = faker.lorem.word());
    let projName = faker.lorem.word();
    let projectEntity = {
      name: projName,
      owners: [faker.internet.email()],
      files: files
    };

    await jest.spyOn(schema.Project, 'findOneAndUpdate')
      .mockImplementationOnce((query, upd, opts, cb) => {
        cb(' ');
      });

    try {
      await ownerService.removeFiles(files.slice(5, 10), ' ');
    } catch (e) {
      expect(e)
        .toEqual(`error while removing files ${files.slice(5, 10)}:  `);
    }
    done();
  });
  it('should succeed to remove project', async (done) => {
    expect.assertions(1);

    // setup
    let files = new Array(50).fill('')
      .map(f => f = faker.lorem.word());
    let projName = faker.lorem.word();
    let projectEntity = {
      name: projName,
      owners: [faker.internet.email()],
      files: files
    };

    await jest.spyOn(schema.Project, 'findOneAndUpdate')
      .mockImplementationOnce((query, upd, opts, cb) => {
        cb();
      });
    try {
      var res = await ownerService.removeFiles(files.slice(5, 10), ' ');
    } catch (e) {
      done.fail(`should succeed, but returns error: ${e.message}`);
    }
    expect(res)
      .toEqual(files.slice(5, 10));
    done();
  });
});

describe('remove project', () => {
  it('should fail to remove project', async function (done) {
    expect.assertions(1);

    // setup
    let projName = faker.lorem.word();

    await jest.spyOn(schema.Project, 'deleteOne')
      .mockImplementation(({}, cb) => {
        cb(' ');
      });

    try {
      await ownerService.removeProject(projName);
    } catch (e) {
      expect(e)
        .toEqual(`error while removing project ${projName}:  `);
    }
    done();
  });
  it('should succeed to remove project', async function (done) {
    expect.assertions(1);

    // setup
    let projName = faker.lorem.word();

    await jest.spyOn(schema.Project, 'deleteOne')
      .mockImplementation(({}, cb) => {
        cb();
      });

    try {
      var res = await ownerService.removeProject(projName);
    } catch (e) {
      done.fail(`should succeed, but returns error: ${e.message}`);
    }
    expect(res)
      .toEqual(projName);
    done();
  });
});

describe('get project', () => {
  it('should fail to get project by id', async function (done) {
    expect.assertions(1);

    // setup
    let projName = faker.lorem.word();

    await jest.spyOn(schema.Project, 'findOne')
      .mockImplementation(({}, cb) => {
        cb(' ');
      });

    try {
      await foreignerService.getProject(projName);
    } catch (e) {
      expect(e)
        .toEqual(`error while getting project ${projName}:  `);
    }
    done();
  });
  it('should succeed to get project by id', async function (done) {
    expect.assertions(1);

    // setup
    let projName = faker.lorem.word();

    await jest.spyOn(schema.Project, 'findOne')
      .mockImplementation(({}, cb) => {
        cb(null, { name: projName });
      });

    try {
      var res = await foreignerService.getProject(projName);
    } catch (e) {
      done.fail(`should succeed, but returns error: ${e.message}`);
    }
    expect(res)
      .toEqual({ name: projName });
    done();
  });
});

describe('parameterizedTests', () => {
  describe('validateProject', () => {
    it('validate project name len: less, than lower boundary, boundary val', async function (done) {
      let name = 'a';
      validator.validateProject({
        name: name
      })
        .catch((e) => {
          expect(e)
            .toEqual(`project name error: length of ${name} have to be between 2 and 20, but 1 given`);
          done();
        });
    });
    it('validate owners len: less, than lower boundary, boundary val', async function (done) {
      let name = 'abc';
      let ownerName = 'ab';
      validator.validateProject({
        name: name,
        owners: [ownerName]
      })
        .catch((e) => {
          expect(e)
            .toEqual(`owner name error: length of ${ownerName} have to be between 3 and 20, but 2 given`);
          done();
        });
    });
  });
  describe('validateEmail', () => {
    it('validate email len: less, than lower boundary, boundary val', async function (done) {
      let str = 'a@a.c';
      let res = validator.validateEmailLength(str);
      expect(res)
        .toEqual(`length of ${str} have to be between 6 and 30, but 5 given`);
      done();
    });
    it('validate email len: less, than lower boundary, equivalent val', async function (done) {
      let str = 'An';
      let res = validator.validateEmailLength(str);
      expect(res)
        .toEqual(`length of ${str} have to be between 6 and 30, but 2 given`);
      done();
    });
    it('validate email len: more, than upper boundary, boundary val', async function (done) {
      let str = 'ivan.smaliakou@emailexample.com';
      let res = validator.validateEmailLength(str);
      expect(res)
        .toEqual(`length of ${str} have to be between 6 and 30, but 31 given`);
      done();
    });
    it('validate email len: more, than upper boundary, equivalent val', async function (done) {
      let str = 'alexandro.chipolino@emailexample.com';
      let res = validator.validateEmailLength(str);
      expect(res)
        .toEqual(`length of ${str} have to be between 6 and 30, but 36 given`);
      done();
    });
    it('validate email len: validation passed, lower boundary val', async function (done) {
      let str = 'i@ya.ru';
      let res = validator.validateEmailLength(str);
      expect(res)
        .toEqual(undefined);
      done();
    });
    it('validate email len: validation passed, upper boundary val', async function (done) {
      let str = 'alexandro.chipolino@exampl.ru';
      let res = validator.validateEmailLength(str);
      expect(res)
        .toEqual(undefined);
      done();
    });
    it('validate email len: validation passed, equivalent val', async function (done) {
      let str = 'ivan.smaliakou@icloud.com';
      let res = validator.validateEmailLength(str);
      expect(res)
        .toEqual(undefined);
      done();
    });
  });

  describe('password', () => {
    it('validate password len: less, than lower boundary, boundary val', async function (done) {
      let str = '123';
      let res = validator.validatePasswordLength(str);
      expect(res)
        .toEqual(`length of ${str} have to be between 4 and 20, but 3 given`);
      done();
    });
    it('validate password len: less, than lower boundary, equivalent val', async function (done) {
      let str = 'a';
      let res = validator.validatePasswordLength(str);
      expect(res)
        .toEqual(`length of ${str} have to be between 4 and 20, but 1 given`);
      done();
    });
    it('validate password len: more, than upper boundary, boundary val', async function (done) {
      let str = 'abc123def456ghjh6789A';
      let res = validator.validatePasswordLength(str);
      expect(res)
        .toEqual(`length of ${str} have to be between 4 and 20, but 21 given`);
      done();
    });
    it('validate email len: more, than upper boundary, equivalent val', async function (done) {
      let str = 'abc123def456ghjh6789Appendix45';
      let res = validator.validatePasswordLength(str);
      expect(res)
        .toEqual(`length of ${str} have to be between 4 and 20, but 30 given`);
      done();
    });
    it('validate password len: validation passed, lower boundary val', async function (done) {
      let str = '1234';
      let res = validator.validatePasswordLength(str);
      expect(res)
        .toEqual(undefined);
      done();
    });
    it('validate password len: validation passed, upper boundary val', async function (done) {
      let str = 'abc123def456ghjh6789';
      let res = validator.validatePasswordLength(str);
      expect(res)
        .toEqual(undefined);
      done();
    });
    it('validate password len: validation passed, equivalent val', async function (done) {
      let str = 'qwerty';
      let res = validator.validatePasswordLength(str);
      expect(res)
        .toEqual(undefined);
      done();
    });
  });

  describe('projectName', () => {
    it('validate project name len: less, than lower boundary, boundary val', async function (done) {
      let str = 'b';
      let res = validator.validateProjectLength(str);
      expect(res)
        .toEqual(`length of ${str} have to be between 2 and 20, but 1 given`);
      done();
    });
    it('validate project name len: less, than lower boundary, equivalent val', async function (done) {
      let str = '';
      let res = validator.validateProjectLength(str);
      expect(res)
        .toEqual(`length of ${str} have to be between 2 and 20, but 0 given`);
      done();
    });
    it('validate project name len: more, than upper boundary, boundary val', async function (done) {
      let str = 'publishEnvironmental1';
      let res = validator.validateProjectLength(str);
      expect(res)
        .toEqual(`length of ${str} have to be between 2 and 20, but 21 given`);
      done();
    });
    it('validate project name len: more, than upper boundary, equivalent val', async function (done) {
      let str = 'destructionDisappointment';
      let res = validator.validateProjectLength(str);
      expect(res)
        .toEqual(`length of ${str} have to be between 2 and 20, but 25 given`);
      done();
    });
    it('validate project name len: validation passed, lower boundary val', async function (done) {
      let str = 'abc';
      let res = validator.validateProjectLength(str);
      expect(res)
        .toEqual(undefined);
      done();
    });
    it('validate project name len: validation passed, upper boundary val', async function (done) {
      let str = 'campaignCompensation';
      let res = validator.validateProjectLength(str);
      expect(res)
        .toEqual(undefined);
      done();
    });
    it('validate project name len: validation passed, equivalent val', async function (done) {
      let str = 'laborer';
      let res = validator.validateProjectLength(str);
      expect(res)
        .toEqual(undefined);
      done();
    });
  });

  describe('ownerName', () => {
    it('validate owner name len: less, than lower boundary, boundary val', async function (done) {
      let str = 'Ra';
      let res = validator.validateOwnerLength(str);
      expect(res)
        .toEqual(`length of ${str} have to be between 3 and 20, but 2 given`);
      done();
    });
    it('validate owner name len: less, than lower boundary, equivalent val', async function (done) {
      let str = 'A';
      let res = validator.validateOwnerLength(str);
      expect(res)
        .toEqual(`length of ${str} have to be between 3 and 20, but 1 given`);
      done();
    });
    it('validate owner name len: more, than upper boundary, boundary val', async function (done) {
      let str = 'attachmentExperienced';
      let res = validator.validateOwnerLength(str);
      expect(res)
        .toEqual(`length of ${str} have to be between 3 and 20, but 21 given`);
      done();
    });
    it('validate owner name len: more, than upper boundary, equivalent val', async function (done) {
      let str = 'attachmentCorrespondence';
      let res = validator.validateOwnerLength(str);
      expect(res)
        .toEqual(`length of ${str} have to be between 3 and 20, but 24 given`);
      done();
    });
    it('validate owner name len: validation passed, lower boundary val', async function (done) {
      let str = 'abc';
      let res = validator.validateOwnerLength(str);
      expect(res)
        .toEqual(undefined);
      done();
    });
    it('validate owner name len: validation passed, upper boundary val', async function (done) {
      let str = 'campaignCompensation';
      let res = validator.validateOwnerLength(str);
      expect(res)
        .toEqual(undefined);
      done();
    });
    it('validate owner name len: validation passed, equivalent val', async function (done) {
      let str = 'disagree';
      let res = validator.validateOwnerLength(str);
      expect(res)
        .toEqual(undefined);
      done();
    });
  });

  describe('validateFiles', () => {
    it('file keys len is more than required', async function (done) {
      let files = [{
        'a': 1,
        'b': 2,
        'c': 3,
        'd': 4
      }];
      try {
        await validator.validateFiles(files);
      } catch (e) {
        expect(e)
          .toEqual('file must have 3 fields, but has 4');
      }
      done();
    });
    it('file keys len is less than required', async function (done) {
      let files = [{
        'a': 1,
        'b': 2,
      }];
      try {
        await validator.validateFiles(files);
      } catch (e) {
        expect(e)
          .toEqual('file must have 3 fields, but has 2');
      }
      done();
    });
    it(`path is not user's home dir path`, async function (done) {
      let filename = '/bin/var/script.lua';
      let files = [{
        'name': filename,
        'b': 2,
        'c': 3
      }];
      try {
        await validator.validateFiles(files);
      } catch (e) {
        expect(e)
          .toEqual('directory path is invalid');
      }
      done();
    });
    it('dirname is dot-prefixed (dir is hidden)', async function (done) {
      let pathToUserDir = require.resolve('./unit.test')
        .split('/')
        .slice(1, 3);
      let filename = '/' + pathToUserDir.join('/') + '/.tmp/script.lua';
      let files = [{
        'name': filename,
        'createdAt': '15-04-2009 12:45:33',
        'updatedAt': '15-04-2009 12:45:33'
      }];
      try {
        await validator.validateFiles(files);
      } catch (e) {
        expect(e)
          .toEqual('directory path is invalid');
      }
      done();
    });
    it('createdAt has invalid date format', async function (done) {
      let pathToUserDir = require.resolve('./unit.test')
        .split('/')
        .slice(1, 3);
      let filename = '/' + pathToUserDir.join('/') + '/script.lua';
      let files = [{
        'name': filename,
        'createdAt': '15-04-2009 12:45:33abc',
        'updatedAt': '15-04-2009 12:45:33abc'
      }];
      try {
        await validator.validateFiles(files);
      } catch (e) {
        expect(e)
          .toEqual('createdAt has invalid date format');
      }
      done();
    });
    it('updatedAt has invalid date format', async function (done) {
      let pathToUserDir = require.resolve('./unit.test')
        .split('/')
        .slice(1, 3);
      let filename = '/' + pathToUserDir.join('/') + '/script.lua';
      let files = [{
        'name': filename,
        'createdAt': '15-04-2009 12:45:33',
        'updatedAt': '15-04-2009 12:45:33abc'
      }];
      try {
        await validator.validateFiles(files);
      } catch (e) {
        expect(e)
          .toEqual('updatedAt has invalid date format');
      }
      done();
    });
    it('success', async function (done) {
      let pathToUserDir = require.resolve('./unit.test')
        .split('/')
        .slice(1, 3);
      let filename = '/' + pathToUserDir.join('/') + '/script.lua';
      let files = [{
        'name': filename,
        'createdAt': '15-04-2009 12:45:33',
        'updatedAt': '15-04-2009 12:45:34'
      }];
      try {
        await validator.validateFiles(files);
      } catch (e) {
        expect(e)
          .toEqual(null);
      }
      done();
    });
  });
  describe('middleware', () => {
    it('should fail to find one with 500 error code', async done => {
      let req = {
        url: '/test'
      };
      jest.spyOn(schema.Project, 'findOne')
        .mockImplementationOnce((obj, cb) => {
          cb(Error());
        });
      let res = {};
      middleware.ownersOnly(req, null, (obj) => {
        res = obj;
      });
      expect(res.message)
        .toEqual('couldn\'t get project by name test: Error');
      done();
    });

    it('should fail because of empty project name', async done => {
      let req = {
        url: '/'
      };
      let res = {};
      middleware.ownersOnly(req, null, (obj) => {
        res = obj;
      });
      expect(res.message)
        .toEqual('empty project name provided');
      done();
    });
  });
});
