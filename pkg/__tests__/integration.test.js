const request = require('supertest');
const mongoose = require('mongoose');
const faker = require('faker');
const sha = require('crypto-js/sha256');
const jwt = require('jsonwebtoken');

const schema = require('../model/schema');
const api = require('../../cmd/index');
const foreignerService = require('../service/foreigner');
const ownerService = require('../service/owner');

require('dotenv');

afterEach(async () => {
  try {
    await schema.User.collection.drop();
    await schema.Project.collection.drop();
  } catch (e) {
    if (e.message !== 'ns not found') {
      throw e;
    }
  }
});

beforeAll(done => {
  done();
});

afterAll(done => {
  // Closing the DB connection allows Jest to exit successfully.
  mongoose.connection.close();
  api.server.close();
  jest.restoreAllMocks();
  done();
});

describe('register user', () => {
  it('should fail during validation', (done) => {
    request(api.app)
      .post('/auth/register')
      .send({
        email: faker.lorem.word(),
        password: faker.lorem.word()
      })
      .expect(400)
      .end(function (err) {
        if (err) return done(err);
        done();
      });
  });
  it('should find existing user and fail', async (done) => {
    // setup
    let email = faker.internet.email();
    await schema.User({ email: email })
      .save();

    await request(api.app)
      .post('/auth/register')
      .send({
        email: email,
        password: faker.lorem.word()
      })
      .expect(400)
      .then(res => {
        expect(res.body.error.message)
          .toEqual('user already exists');
        done();
      });
  });
  it('should fail to find user with 500 error code', async (done) => {
    let email = faker.internet.email();
    jest.spyOn(schema.User, 'findOne')
      .mockImplementationOnce((email, id) => {
        return {
          exec: () => {
            return Promise.reject('');
          }
        };
      });
    await request(api.app)
      .post('/auth/register')
      .send({
        email: email,
        password: faker.lorem.word()
      })
      .expect(500)
      .then(res => {
        expect(res.body.error.message)
          .toEqual('cannot find user entity: ');
        done();
      });
  });
  it('should fail to save user with 500 error code', async (done) => {
    let email = faker.internet.email();
    let password = faker.lorem.word();
    const passwordHash = sha(password)
      .toString();

    jest.spyOn(schema.User, 'findOne')
      .mockImplementationOnce((email, id) => {
        return {
          exec: () => {
            return Promise.resolve(null);
          }
        };
      });
    jest.spyOn(schema.User.prototype, 'save')
      .mockImplementationOnce(() => {
        return Promise.reject('');
      });
    await request(api.app)
      .post('/auth/register')
      .send({
        email: email,
        password: faker.lorem.word()
      })
      .expect(500)
      .then(res => {
        expect(res.body.error.message)
          .toEqual('failed to save user: ');
        done();
      });
  });
  it('should register and return saved user', async (done) => {
    let email = faker.internet.email();
    await request(api.app)
      .post('/auth/register')
      .send({
        email: email,
        password: faker.lorem.word()
      })
      .expect(200)
      .then(res => {
        expect(res.body.user.email)
          .toEqual(email);
        done();
      });
  });
});

describe('login', () => {
  it('should fail during validation', async (done) => {
    const res = await request(api.app)
      .post('/auth/login')
      .send({
        email: faker.lorem.word(),
        password: faker.lorem.word()
      });
    expect(res.statusCode)
      .toEqual(400);
    expect(res.body.error['0'].location)
      .toEqual('body');
    done();
  });
  it('should not find user', async (done) => {
    let email = faker.internet.email();
    const res = await request(api.app)
      .post('/auth/login')
      .send({
        email: email,
        password: faker.lorem.word()
      });
    expect(res.statusCode)
      .toEqual(400);
    expect(res.body.error.message)
      .toEqual(`user with email ${email} does not exist`);
    done();
  });
  it('should fail due to different passwords', async (done) => {
    // setup
    let email = faker.internet.email();
    await schema.User({
      email: email,
      passwordSHA: sha(faker.lorem.word())
    })
      .save();

    const res = await request(api.app)
      .post('/auth/login')
      .send({
        email: email,
        password: faker.lorem.word()
      });
    expect(res.statusCode)
      .toEqual(403);
    expect(res.body.error.message)
      .toEqual(`invalid password provided`);
    done();
  });
  it('should fail to find user by id and return 500 err code', async (done) => {
    // setup
    let email = faker.internet.email();
    let password = faker.lorem.word();
    await schema.User({
      email: email,
      passwordSHA: sha(password)
    })
      .save();
    jest.spyOn(schema.User, 'findOne').mockImplementationOnce((obj) => {
      return Promise.reject('');
    })
    const res = await request(api.app)
      .post('/auth/login')
      .send({
        email: email,
        password: password
      });
    expect(res.statusCode)
      .toEqual(500);
    expect(res.body.error.message)
      .toEqual('cannot find user entity: ');
    done();
  });
  it('should successfully log in and return jwt', async (done) => {
    // setup
    let email = faker.internet.email();
    let password = faker.lorem.word();
    await schema.User({
      email: email,
      passwordSHA: sha(password)
    })
      .save();

    const res = await request(api.app)
      .post('/auth/login')
      .send({
        email: email,
        password: password
      });
    expect(res.statusCode)
      .toEqual(200);
    expect(res.body.token)
      .toEqual(jwt.sign({ email: email }, process.env.TOKEN_SECRET, { expiresIn: process.env.TOK_EXPIRES_IN }));
    done();
  });
});

describe('get project by id', () => {
  it('should fail to find private project', (done) => {
    //setup
    let projectName = faker.lorem.word();
    schema.Project({
      name: faker.lorem.word(),
      isPublic: false,
      owners: faker.internet.email
    })
      .save();

    request(api.app)
      .get(`/v1/${projectName}`)
      .expect(404)
      .then(res => {
        expect(res.body.error.message)
          .toEqual('project is private or does not exist');
        done();
      });
  });

  it('should fail with 500 internal server error', (done) => {
    //setup
    let projectName = faker.lorem.word();
    jest.spyOn(foreignerService, 'getProject')
      .mockImplementationOnce((id) => {
        return Promise.reject('');
      });

    request(api.app)
      .get(`/v1/${projectName}`)
      .expect(500)
      .then(res => {
        expect(res.body.error.message)
          .toEqual('Internal Server Error');
        done();
      });
  });

  it('should fail because there is no such project', async (done) => {
    //setup
    let projectName = faker.lorem.word();
    let projectEntity = await schema.Project({
      name: projectName,
      isPublic: true,
      owners: faker.internet.email
    })
      .save();

    request(api.app)
      .get(`/v1/${projectName}`)
      .expect(200)
      .then(res => {
        expect(res.body.project.id)
          .toEqual(projectEntity.id);
        done();
      });
  });
});

describe('init new project', () => {
  it('should fail auth', async (done) => {
    // setup
    let userEntity = schema.User({
      email: faker.internet.email(),
      password: sha(faker.lorem.word())
    })
      .save();

    const res = await request(api.app)
      .post(`/v1/init`)
      .set('authorization', `Bearer ${faker.lorem.word()}`)
      .send({});
    expect(res.statusCode)
      .toEqual(403);
    expect(res.body.error.message)
      .toEqual('token verification has been failed: JsonWebTokenError: jwt malformed');
    done();
  });

  it('should fail because project exists', async (done) => {
    // setup
    let projName = faker.lorem.word();
    let projectEntity = await schema.Project({
      name: projName,
    })
      .save();

    let res = await request(api.app)
      .post(`/v1/init`)
      .set('Authorization', `Bearer ${jwt.sign({ email: faker.internet.email }, process.env.TOKEN_SECRET, { expiresIn: process.env.TOK_EXPIRES_IN })}`)
      .send({
        name: projName,
        isPublic: true,
        owners: ['abc@mail.ru', 'defee@mail.ru']
      });

    expect(res.statusCode)
      .toEqual(500);
    expect(res.body.error.message)
      .toEqual(`failed to init new project: project ${projName} already exists`);
    done();
  });

  it('should fail to validate project', async (done) => {
    // setup
    let projName = faker.lorem.word(1);

    let res = await request(api.app)
      .post(`/v1/init`)
      .set('Authorization', `Bearer ${jwt.sign({ email: faker.internet.email }, process.env.TOKEN_SECRET, { expiresIn: process.env.TOK_EXPIRES_IN })}`)
      .send({
        name: projName,
        isPublic: true,
        owners: ['abc@mail.ru', 'defee@mail.ru']
      });

    expect(res.statusCode)
      .toEqual(400);
    expect(res.body.error.message)
      .toEqual('validation error: project name error: length of a have to be between 2 and 20, but 1 given');
    done();
  });

  it('should succeed to create new project', async (done) => {
    // setup
    let projName = faker.lorem.word();

    let res = await request(api.app)
      .post(`/v1/init`)
      .set('Authorization', `Bearer ${jwt.sign({ email: faker.internet.email }, process.env.TOKEN_SECRET, { expiresIn: process.env.TOK_EXPIRES_IN })}`)
      .send({
        name: projName,
        isPublic: true,
        owners: ['abc@mail.ru', 'defee@mail.ru']
      });

    expect(res.statusCode)
      .toEqual(200);
    expect(res.body.project.name)
      .toEqual(projName);
    done();
  });

  it('should succeed to create new project with files', async (done) => {
    // setup
    let projName = faker.lorem.word();

    let res = await request(api.app)
      .post(`/v1/init`)
      .set('Authorization', `Bearer ${jwt.sign({ email: faker.internet.email }, process.env.TOKEN_SECRET, { expiresIn: process.env.TOK_EXPIRES_IN })}`)
      .send({
        name: projName,
        isPublic: true,
        owners: ['abc@mail.ru', 'defee@mail.ru'],
        files: [{
          name: faker.lorem.word(),
          createdAt: faker.date.past(),
          updatedAt: faker.date.past()
        }],
      });

    expect(res.statusCode)
      .toEqual(200);
    expect(res.body.project.name)
      .toEqual(projName);
    done();
  });
});

describe('apply files', () => {
  it('should fail because email extracted from token is not owner of the project', async (done) => {
    // setup
    let projName = faker.lorem.word(10);
    let projectEntity = await schema.Project({
      name: projName,
      owners: ['abc@mail.ru', 'defee@mail.ru']
    })
      .save();

    let res = await request(api.app)
      .post(`/v1/${projName}/apply`)
      .set('Authorization', `Bearer ${jwt.sign({ email: 'faker@email.com' }, process.env.TOKEN_SECRET, { expiresIn: process.env.TOK_EXPIRES_IN })}`)
      .send({
        files: [{
          name: faker.lorem.word(),
          createdAt: faker.date.past(),
          updatedAt: faker.date.past(),
        }]
      });

    expect(res.statusCode)
      .toEqual(403);
    expect(res.body.error.message)
      .toEqual('not enough permissions to access this resource');
    done();
  });

  it('should fail to validate files', async (done) => {
    // setup
    let filename = async () => {
      let pathToUserDir = require.resolve('./integration.test')
        .split('/')
        .slice(1, 3);
      return '/' + pathToUserDir.join('/') + '/.tmp/script.lua';
    };

    let projName = faker.lorem.word(10);
    let userEntity = await schema.User({
      email: 'defee@mail.ru',
      password: faker.lorem.word(10)
    })
      .save();

    let projectEntity = await schema.Project({
      name: projName,
      owners: ['defee@mail.ru'],
    })
      .save();

    let res = await request(api.app)
      .post(`/v1/${projName}/apply`)
      .set('Authorization', `Bearer ${jwt.sign({ email: userEntity.email }, process.env.TOKEN_SECRET, { expiresIn: process.env.TOK_EXPIRES_IN })}`)
      .send({
        files: [{
          name: await filename(),
          createdAt: '11-09-2020 14:39:10',
          updatedAt: '11-09-2020 14:39:10',
        }]
      });

    expect(res.statusCode)
      .toEqual(400);
    expect(res.body.error.message)
      .toEqual('validation error: directory path is invalid');
    done();
  });

  it('should fail with 500 internal server error', async (done) => {
    // setup
    let filename = async () => {
      let pathToUserDir = require.resolve('./integration.test')
        .split('/')
        .slice(1, 3);
      return '/' + pathToUserDir.join('/') + '/script.lua';
    };

    let projName = faker.lorem.word(10);
    let userEntity = await schema.User({
      email: 'defee@mail.ru',
      password: faker.lorem.word(10)
    })
      .save();

    await schema.Project({
      name: projName,
      owners: ['defee@mail.ru'],
    })
      .save();

    jest.spyOn(ownerService, 'apply')
      .mockImplementationOnce((files, name) => {
        return Promise.reject('a');
      });

    let res = await request(api.app)
      .post(`/v1/${projName}/apply`)
      .set('Authorization', `Bearer ${jwt.sign({ email: userEntity.email }, process.env.TOKEN_SECRET, { expiresIn: process.env.TOK_EXPIRES_IN })}`)
      .send({
        files: [{
          name: await filename(),
          createdAt: '11-09-2020 14:39:10',
          updatedAt: '11-09-2020 14:39:10',
        }]
      });

    expect(res.statusCode)
      .toEqual(500);
    expect(res.body.error.message)
      .toEqual('failed to apply files: a');
    done();
  });

  it('should succeed to apply files', async (done) => {
    // setup
    let filename = async () => {
      let pathToUserDir = require.resolve('./integration.test')
        .split('/')
        .slice(1, 3);
      return '/' + pathToUserDir.join('/') + '/script.lua';
    };

    let projName = faker.lorem.word(10);
    let userEntity = await schema.User({
      email: 'defee@mail.ru',
      password: faker.lorem.word(10)
    })
      .save();

    let projectEntity = await schema.Project({
      name: projName,
      owners: ['defee@mail.ru'],
    })
      .save();

    let res = await request(api.app)
      .post(`/v1/${projName}/apply`)
      .set('Authorization', `Bearer ${jwt.sign({ email: userEntity.email }, process.env.TOKEN_SECRET, { expiresIn: process.env.TOK_EXPIRES_IN })}`)
      .send({
        files: [{
          name: await filename(),
          createdAt: '11-09-2020 14:39:10',
          updatedAt: '11-09-2020 14:39:10',
        }]
      });

    expect(res.statusCode)
      .toEqual(200);
    done();
  });
});

describe('delete files', () => {
  it('should fail because email extracted from token is not owner of the project', async (done) => {
    // setup
    let projName = faker.lorem.word(10);
    let projectEntity = await schema.Project({
      name: projName,
      owners: ['defee@mail.ru'],
    })
      .save();

    let res = await request(api.app)
      .delete(`/v1/${projName}/files`)
      .set('Authorization', `Bearer ${jwt.sign({ email: 'faker@email.com' }, process.env.TOKEN_SECRET, { expiresIn: process.env.TOK_EXPIRES_IN })}`)
      .send({
        files: [faker.lorem.word(10)]
      });

    expect(res.statusCode)
      .toEqual(403);
    expect(res.body.error.message)
      .toEqual('not enough permissions to access this resource');
    done();
  });

  it('should fail with 500 internal server error', async (done) => {
    // setup
    let pathToUserDir = require.resolve('./integration.test')
      .split('/')
      .slice(1, 3);
    let filename = '/' + pathToUserDir.join('/') + '/script.lua';

    let projName = faker.lorem.word(10);
    let userEntity = await schema.User({
      email: 'defee@mail.ru',
      password: faker.lorem.word(10)
    })
      .save();

    jest.spyOn(ownerService, 'removeFiles')
      .mockImplementationOnce((files, name) => {
        return Promise.reject('');
      });
    let projectEntity = await schema.Project({
      name: projName,
      owners: [userEntity.email],
    })
      .save();

    let res = await request(api.app)
      .delete(`/v1/${projName}/files`)
      .set('Authorization', `Bearer ${jwt.sign({ email: userEntity.email }, process.env.TOKEN_SECRET, { expiresIn: process.env.TOK_EXPIRES_IN })}`)
      .send({
        files: [filename]
      });

    expect(res.statusCode)
      .toEqual(500);
    expect(res.body.error.message)
      .toEqual(`cannot remove files from project ${projName}: `);
    done();
  });

  it('should succeed to delete files', async (done) => {
    // setup
    let pathToUserDir = require.resolve('./integration.test')
      .split('/')
      .slice(1, 3);
    let filename = '/' + pathToUserDir.join('/') + '/script.lua';

    let projName = faker.lorem.word(10);
    let userEntity = await schema.User({
      email: 'defee@mail.ru',
      password: faker.lorem.word(10)
    })
      .save();

    let projectEntity = await schema.Project({
      name: projName,
      owners: [userEntity.email],
    })
      .save();

    let res = await request(api.app)
      .delete(`/v1/${projName}/files`)
      .set('Authorization', `Bearer ${jwt.sign({ email: userEntity.email }, process.env.TOKEN_SECRET, { expiresIn: process.env.TOK_EXPIRES_IN })}`)
      .send({
        files: [filename]
      });

    expect(res.statusCode)
      .toEqual(200);
    done();
  });
});

describe('delete project', () => {
  it('should fail because project does not exist', async (done) => {
    // setup
    let projName = faker.lorem.word(10);

    let res = await request(api.app)
      .delete(`/v1/${projName}`)
      .set('Authorization', `Bearer ${jwt.sign({ email: 'faker@email.com' }, process.env.TOKEN_SECRET, { expiresIn: process.env.TOK_EXPIRES_IN })}`)
      .send({
        files: [faker.lorem.word(10)]
      });

    expect(res.statusCode)
      .toEqual(400);
    expect(res.body.error.message)
      .toEqual(`wrong project name: ${projName}`);
    done();
  });

  it('should fail with 500 internal server error', async (done) => {
    // setup
    let projName = faker.lorem.word(10);
    let userEntity = await schema.User({
      email: 'defee@mail.ru',
      password: faker.lorem.word(10)
    })
      .save();

    let projectEntity = await schema.Project({
      name: projName,
      owners: [userEntity.email],
    })
      .save();

    jest.spyOn(ownerService, 'removeProject')
      .mockImplementationOnce((files, name) => {
        return Promise.reject('');
      });

    let res = await request(api.app)
      .delete(`/v1/${projName}`)
      .set('Authorization', `Bearer ${jwt.sign({ email: userEntity.email }, process.env.TOKEN_SECRET, { expiresIn: process.env.TOK_EXPIRES_IN })}`)
      .send({
        files: [faker.lorem.word()]
      });

    expect(res.statusCode)
      .toEqual(500);
    expect(res.body.error.message)
      .toEqual('cannot remove project: ');

    done();
  });

  it('should succeed to delete project', async (done) => {
    // setup
    let projName = faker.lorem.word(10);
    let userEntity = await schema.User({
      email: 'defee@mail.ru',
      password: faker.lorem.word(10)
    })
      .save();

    let projectEntity = await schema.Project({
      name: projName,
      owners: [userEntity.email],
    })
      .save();

    let res = await request(api.app)
      .delete(`/v1/${projName}`)
      .set('Authorization', `Bearer ${jwt.sign({ email: userEntity.email }, process.env.TOKEN_SECRET, { expiresIn: process.env.TOK_EXPIRES_IN })}`)
      .send({
        files: [faker.lorem.word()]
      });

    expect(res.statusCode)
      .toEqual(200);
    done();
  });
});
