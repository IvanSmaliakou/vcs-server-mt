const schema = require('../model/schema');
const h = require('object-hash');
const moment = require('moment');

function apply(files, projName) {
  if (!files || files.length === 0) {
    return Promise.resolve();
  }
  files = files.map(file => {
      return {
        name: file.name,
        createdAt: moment(file.createdAt, "DD-MM-YYYY HH:mm:ss").toDate(),
        updatedAt: moment(file.updatedAt, "DD-MM-YYYY HH:mm:ss").toDate(),
        hash: h(file),
      };
    }
  );
  return new Promise(((resolve, reject) => {
    schema.Project.findOneAndUpdate({ name: projName }, { '$push': { files: files } },
      { useFindAndModify: false }, (err, res) => {
        if (err) {
          return reject(`error while adding files to project: ${err.message}`);
        }
        return resolve(res);
      });
  }));
}

function init(project) {
  return new Promise((resolve, reject) => {
    schema.Project.findOne({ name: project.name }, (err, res) => {
      if (err) {
        return reject(Error(`couldn't get project by name ${project.name}: ${err}`));
      }
      if (res) {
        return reject(Error(`project ${project.name} already exists`));
      }
      project.save((err, savedProject) => {
        if (err) {
          return reject(`error while saving project ${project.name}: ${err.message}`);
        }
        resolve(savedProject);
      });
    });
  });
}

function removeFiles(files, projName) {
  return new Promise((resolve, reject) => {
    schema.Project.findOneAndUpdate({ name: projName }, { '$pull': { files: { name: { '$in': files } } } },
      { useFindAndModify: false }, (err) => {
        if (err) {
          reject(`error while removing files ${files}: ${err}`);
        }
        resolve(files);
      });
  });
}

function removeProject(projectName) {
  return new Promise((resolve, reject) => {
    schema.Project.deleteOne({ name: projectName }, err => {
      if (err) {
        return reject(`error while removing project ${projectName}: ${err}`);
      }
      return resolve(projectName);
    });
  });
}

module.exports = {
  init,
  apply,
  removeFiles,
  removeProject
};
