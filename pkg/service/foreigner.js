const schema = require('../model/schema');

function getProject(projName) {
  return new Promise((resolve, reject) => {
    schema.Project.findOne({ name: projName }, function (err, proj) {
      if (err) {
        reject(`error while getting project ${projName}: ${err}`);
      }
      resolve(proj);
    });
  });
}

module.exports = {
  getProject
};
