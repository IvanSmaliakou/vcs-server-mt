const moment = require('moment');

async function validateProject(project) {
    let validateProjectNameLenErrorMsg = validateProjectLength(project.name);
    if (validateProjectNameLenErrorMsg) {
        return Promise.reject('project name error: ' + validateProjectNameLenErrorMsg);
    }
    for (let ownerEmail of project.owners) {
        let validateOwnerNamesLenErrorMsg = validateOwnerLength(ownerEmail);
        if (validateOwnerNamesLenErrorMsg) {
            return Promise.reject('owner name error: ' + validateOwnerNamesLenErrorMsg);
        }
    }
    return Promise.resolve(null);
}

async function validateFiles(files) {
    for (let file of files) {
        if (Object.keys(file).length !== 3) {
            return Promise.reject(`file must have 3 fields, but has ${Object.keys(file).length}`);
        }
        if (!isValidDirectory(file.name)) {
            return Promise.reject('directory path is invalid');
        }
        if (!moment(file.createdAt, "DD-MM-YYYY HH:mm:ss", true).isValid()) {
            return Promise.reject('createdAt has invalid date format');
        }
        if (!moment(file.updatedAt, "DD-MM-YYYY HH:mm:ss", true).isValid()) {
            return Promise.reject('updatedAt has invalid date format');
        }
    }
    return Promise.resolve(null);
}

function isValidDirectory(filePath) {
    let pathToUserDir = require.resolve('./validator').split('/').slice(1, 3);
    if (!filePath.startsWith(`/${pathToUserDir.join('/')}/`)) {
        return false;
    }
    let splFile = filePath.split('/');
    splFile.splice(-1, 1);
    for (let file of splFile) {
        if (file.startsWith('.')) {
            return false;
        }
    }
    return true;
}

function validateLen(str, min, max) {
    let strLen = str.length;
    if (strLen > max || strLen < min) {
        return `length of ${str} have to be between ${min} and ${max}, but ${strLen} given`;
    }
}

function validateEmailLength(email){
    return validateLen(email, 6, 30);
}

function validatePasswordLength(pass){
  return validateLen(pass, 4, 20);
}

function validateProjectLength(proj){
  return validateLen(proj, 2, 20);
}

function validateOwnerLength(ownerName){
    return validateLen(ownerName, 3, 20);
}

module.exports = {
    validateProject,
    validateFiles,
    validateOwnerLength,
    validateEmailLength,
    validatePasswordLength,
    validateProjectLength
}
