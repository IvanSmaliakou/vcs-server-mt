const express = require('express');
const authHandler = require('../pkg/api/auth');
const serviceHandler = require('../pkg/api/service');
const middleware = require('../pkg/api/middleware');
const bodyParser = require('body-parser');
require('express-group-routes');
require('dotenv')
  .config();

const app = express();
const port = process.env.PORT;

app.use(bodyParser.json());

let server = app.listen(port, () => {
  console.log(`server is listening on http://localhost:${port}`);
});

app.group('/auth', authHandler);
app.group('/v1', serviceHandler.allUsers);
app.use(middleware.handleErrors);

module.exports = {
  app,
  server
};
